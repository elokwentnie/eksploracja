# Projekt z Eksploracji danych biomedycznych / Głosowej łączności z komputerem 
## Instrukcja

- Instrukcja dostępna jest w dokumencie tekstowym dostępnym [tutaj](https://docs.google.com/document/d/1dxfh30gPJiww0oAJhP-pEEaO5v_3vZO9IC531iE86sA/edit?usp=sharing)

- [ ] Instrukcja znajdzie się w repozytorium

## Punkt 1 : Zbieranie danych
- Każdy z członków nagrywa i przygotowuje dane jak w [instrukcji](https://docs.google.com/document/d/1dxfh30gPJiww0oAJhP-pEEaO5v_3vZO9IC531iE86sA/edit?usp=sharing) i dodaje dane do folderu **dane** do 02.04.2021r. (tj. piątek)
- Po zebraniu danych będziemy rozdzielać dalej zadania itd.

## Członkowie
- Natalia Szczepanek 
- Sara Blichowska 
- Paulina Król 
- Karolina Gutka 
- Jan Migoń 
- Maciej Okapa 
- Maciej Rakocki 
- Mateusz Marynowski 
- Weronika Celniak 
- Weronika Węgrzyn 
